#include "server.h"

#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include "bool.h"

void start(int port) {
	int server_fd, new_socket, valread;
	struct sockaddr_in address;
	int opt = 1;
	int addrlen = sizeof(address);
	char bufferIn[BUFFER_SIZE] = {0};
	char bufferOut[BUFFER_SIZE] = {0};

	strcpy(bufferOut, "coucou");

	if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
		perror("socket failed");
		exit(EXIT_FAILURE);
	}

	if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(port);

	if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
		perror("bind failed");
		exit(EXIT_FAILURE);
	}
	if (listen(server_fd, 3) < 0) {
		perror("listen");
		exit(EXIT_FAILURE);
	}
	printf("Listening on port %d\n", port);

	while (true) {

		if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0) {
			perror("accept");
			exit(EXIT_FAILURE);
		}

        printf("Client connected\n");

		while (true) {
			memset(bufferIn, 0, BUFFER_SIZE);
			memset(bufferOut, 0, BUFFER_SIZE);

			recv(new_socket, bufferIn, BUFFER_SIZE, 0);
			if(strcmp(bufferIn,"exit") == 0){
                break;
            }
            handleClientRequest(bufferIn, bufferOut);
			printf("CLIENT : %s\n", bufferIn);
			printf("SERVER : %s\n", bufferOut);
			send(new_socket, bufferOut, BUFFER_SIZE, 0);
		}
		printf("Client disconnected\n\n");
        close(new_socket);
	}
    close(server_fd);
}

void handleClientRequest(char *bufferIn, char *bufferOut) {
	switch (bufferIn[0]) {
	case '1':
		handleClientRequestSize(bufferIn, bufferOut);
		break;

	case '2':
		handleClientRequestSort(bufferIn, bufferOut);
		break;
	}
}

void handleClientRequestSize(char *bufferIn, char *bufferOut) {
	int len = strlen(bufferIn) - 2;
	sprintf(bufferOut, "%d", len);
}

void handleClientRequestSort(char *bufferIn, char *bufferOut) {
	strcpy(bufferOut, bufferIn + 2);
	sort(bufferOut);
}

void swap(char *a, char *b) {
	char tmp = *a;
	*a = *b;
	*b = tmp;
}

void sort(char *word) {
	int len = strlen(word);
	for (unsigned int i = 0; i < len - 1; i++)
		for (unsigned int j = i + 1; j < len; j++)
			if (word[i] > word[j])
				swap(&word[i], &word[j]);
}
