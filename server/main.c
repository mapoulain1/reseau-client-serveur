#include <stdio.h>
#include <stdlib.h>

#include "server.h"


void usage(void) {
	printf("usage : \n");
	printf("\t server [port]\n");
}


int main(int argc, char const *argv[])
{
    if(argc != 2){
        usage();
        return -1;
    }
    
    start(atoi(argv[1]));
    
    return 0;
}
