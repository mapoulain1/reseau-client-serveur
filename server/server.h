#ifndef SERVER_H
#define SERVER_H

#define BUFFER_SIZE 1024


void start(int port);


void handleClientRequest(char *bufferIn, char *bufferOut);
void handleClientRequestSize(char * bufferIn, char * bufferOut);
void handleClientRequestSort(char * bufferIn, char * bufferOut);


void swap(char *a, char *b);
void sort(char *word);


#endif