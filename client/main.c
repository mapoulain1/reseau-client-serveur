#include <stdio.h>
#include <stdlib.h>


#include "client.h"


void usage(void) {
	printf("usage : \n");
	printf("\t client [ip] [port]\n");
}

int main(int argc, char const *argv[]) {
	if (argc != 3) {
		usage();
		return -1;
	}

	loop(argv[1], atoi(argv[2]));

	return 0;
}
