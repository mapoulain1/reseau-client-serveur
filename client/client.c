#include "client.h"

#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include "bool.h"

int printMenu(const char *ip) {
	char choice;
	printf("────────────────── %s ─────────────────\n", ip);
	printf("1) Size\n");
	printf("2) Sort\n");
	printf("3) Exit\n");
	scanf("%c%*c", &choice);
	return choice - '0';
}

void loop(const char *ip, int port) {
	int choice = 0;

	char tmp[BUFFER_SIZE / 2] = {0};
	char bufferIn[BUFFER_SIZE] = {0};
	char bufferOut[BUFFER_SIZE] = {0};

	int sock = 0;
	struct sockaddr_in serv_addr;
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		fprintf(stderr,"\n Socket creation error \n");
		return;
	}

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(port);

	if (inet_pton(AF_INET, ip, &serv_addr.sin_addr) <= 0) {
		fprintf(stderr, "Invalid address\n");
		return;
	}

	if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
		fprintf(stderr, "Connection Failed\n");
		return;
	}

	do {
		system("clear");
		memset(bufferIn, 0, BUFFER_SIZE);
		memset(bufferOut, 0, BUFFER_SIZE);

		choice = printMenu(ip);
		if(choice == 3){
			sprintf(bufferOut,"exit");
			send(sock, bufferOut, BUFFER_SIZE, 0);
			printf("Disconnected !\n");
			break;
		}

		fgets(tmp, BUFFER_SIZE / 2, stdin);
		tmp[strlen(tmp) - 1] = '\0';

		sprintf(bufferOut, "%d %s", choice, tmp);

		send(sock, bufferOut, BUFFER_SIZE, 0);
		recv(sock, bufferIn, BUFFER_SIZE, 0);
		printf("Server : %s\n", bufferIn);
		getc(stdin);

	} while (true);
}

