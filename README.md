# Réseau - Client Serveur

## Auteur

Maxime POULAIN - F2 - ZZ2

## Usage

### Serveur

Pour compiler et lancer le serveur : 

```bash
cd server
make
./server 25565

```

### Client - C

Pour compiler et lancer le client C: 

```bash
cd client
make
./client 127.0.0.1 25565

```

### Client - Java

Pour compiler et executer le client java : 

```bash
cd client-java
javac src/Main.java 
java -cp src Main
```

