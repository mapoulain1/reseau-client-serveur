import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    private static void usage() {
        System.out.println("usage : ");
        System.out.println("\t client [ip] [port]");
    }

    private static char menu(String ip) {
        System.out.println("──────────────────" + ip + "─────────────────");
        System.out.println("1) Size");
        System.out.println("2) Sort");
        System.out.println("3) Exit");

        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine().charAt(0);
    }

    private static void loop(String ip, int port) {
        Socket socketOfClient = null;
        BufferedWriter os = null;
        BufferedReader is = null;
        char choice = 0;
        Scanner scanner = new Scanner(System.in);
        char[] buffer = new char[1024];

        try {
            socketOfClient = new Socket(ip, port);
            os = new BufferedWriter(new OutputStreamWriter(socketOfClient.getOutputStream()));
            is = new BufferedReader(new InputStreamReader(socketOfClient.getInputStream()));
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host " + ip);
            return;
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to " + ip);
            return;
        }
        try {
            String responseLine;
            do {
                choice = menu(ip);
                if (choice == '3') {
                    os.write("exit");
                    os.write('\0');
                    os.flush();
                    break;
                }
                os.write(choice);
                os.write(' ');
                os.write(scanner.nextLine());
                os.write('\0');
                os.flush();

                int result = is.read(buffer, 0, 1024);
                System.out.println("Server: " + fromBuffer(buffer));
            } while (true);

            os.close();
            is.close();
            socketOfClient.close();
        } catch (UnknownHostException e) {
            System.err.println("Trying to connect to unknown host: " + e);
        } catch (IOException e) {
            System.err.println("IOException:  " + e);
        }


    }


    private static String fromBuffer(char[] buffer){
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while(i < 1024 && buffer[i] != '\0'){
            sb.append(buffer[i]);
            i++;
        }
        return sb.toString();
    }


    public static void main(String[] args) {
        if (args.length != 2) {
            usage();
            return;
        }
        System.out.println("Connecting to " + args[0] + ":" + args[1]);
        loop(args[0], Integer.parseInt(args[1]));
    }
}
